<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BalanceController extends Controller
{
    public function index(){
        
        $balance = Auth()->user()->balance;
        $amount = $balance ? $balance->amount : 'R$ 0,00';
        
        return view('admin/balance/index',compact('amount'));
    }

    public function deposit(){
        return view('admin/balance/deposit');
    }

    public function depositStore(Request $request){
        dd($request->all());
    }
}
