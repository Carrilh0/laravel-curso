<?php

Route::group(['middleware' => 'auth','prefix' => 'admin'],function(){
    Route::get('/','Admin\AdminController@index')->name('admin/home');

    Route::get('balance','Admin\BalanceController@index')->name('admin/balance');
    
    Route::post('balance/deposit','Admin\BalanceController@depositStore')->name('deposit/store');
    Route::get('balance/deposit','Admin\BalanceController@deposit')->name('balace/deposit');
});



Route::get('/', 'Site\SiteController@index');

Auth::routes();