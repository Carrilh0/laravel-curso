@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Fazer Deposito</h1>
    
    <ol class='breadcrumb'>
            <li><a href='#'>Dashboard</a></li>
            <li><a href='#'>Saldo</a></li>
            <li><a href='#'>Depositar</a></li>
    </ol>
@stop

@section('content')
<div class='box'>
        <div class='box-header'>
            <h3>Fazer Deposito</h3>
        </div>
        <div class='box-body'>

                        <form method='POST' action='{{route('deposit/store')}}'>  
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <input type='text' class="form-control" placeholder="Valor da Recarga">
                            </div>
                            <div class="form-group">
                                    <button type='subit' class='btn btn-danger'>Depositar</button>
                            </div>
                        </form>
        </div>
    </div>
@stop